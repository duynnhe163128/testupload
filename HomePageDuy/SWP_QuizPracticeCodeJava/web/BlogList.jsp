
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>List blogs</h1>
        <table border="1">
            <thead>
                <tr>
                    <th>Blog title</th>
                    <th>Blog category</th>
                    <th>Date</th>
                    <th>Brief info</th>
                    <th>Detail</th>
                    <th>Thumbnail</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${requestScope.listBlog}" var="blog">
                    <tr>
                        <td>${blog.getBlogTitle()}</td>
                        <td>${blog.getCateName(blog.getBlogCategoryId())}</td>
                        <td>${blog.getLastUpdated()}</td>
                        <td>${blog.getBriefInfo()}</td>
                        <td>${blog.getBlogDetail()}</td>
                        <td>
                            <img src="${blog.getThumbnail()}"
                                 width="100"> 
                        </td>
                    </tr>
                </c:forEach>    
            </tbody>
        </table>
    </body>
</html>
