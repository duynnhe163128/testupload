<%-- 
    Document   : RegisterAccount
    Created on : May 24, 2022, 8:35:05 AM
    Author     : Halinh
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Page</title>
    </head>
    <body>
        <form method="post">
            Full name: <input name="fullname" type="text" > <br> <br>
            Username: <input name="username" type="text" > <br> <br>
            Password: <input name="password" type="text" > <br> <br>
            Confirm password: <input name="repassword" type="text" > <br> <br>
            Sex:   
            Female<input name="gender" type="radio" value="female">
            Male<input name="gender" type="radio" value="male">
            <br><br>
            Phone: <input name="phone" type="number"> <br> <br>
            Email: <input name="email" type="email" >
            <button>Submit</button>
        </form>
        <h3>${requestScope.mess}</h3>
        <h3>${requestScope.messError}</h3>           
    </body>
</html>
