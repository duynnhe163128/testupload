
package DAL;

import Models.Quiz;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class QuizDAO extends DBContext{
    public ArrayList<Quiz> getAllQuiz(){
        ArrayList<Quiz> list = new ArrayList<>();
        String sql = "Select * from Quiz";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Quiz quiz = new Quiz(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9));
                list.add(quiz);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }
    
    public static void main(String[] args) {
        QuizDAO dao = new QuizDAO();
        ArrayList<Quiz> listQuiz = dao.getAllQuiz();
        for (Quiz quiz : listQuiz) {
            System.out.println(quiz.getQuizName()+" "+quiz.getQuizContent());
        }
    }
}

