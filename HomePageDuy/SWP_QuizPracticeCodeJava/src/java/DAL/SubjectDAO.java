/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import Models.Category;
import Models.Subject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class SubjectDAO extends DBContext {

    public ArrayList<Subject> getAllSubject() {
        ArrayList<Subject> list = new ArrayList<>();
        String sql = "select Subject.*, Category.* from Subject\n"
                + "join SubjectCategory on Subject.subjectId = SubjectCategory.subjectId\n"
                + "join Category on Category.catId = SubjectCategory.catId\n"
                + "where Subject.featured = 1";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Subject s = new Subject(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getBoolean(5),
                        rs.getBoolean(6),
                        rs.getDate(7),
                        new Category(rs.getInt(8), rs.getString(9)));
                list.add(s);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public static void main(String[] args) {
        SubjectDAO dao = new SubjectDAO();
        System.out.println(dao.getAllSubject());
    }
}
