
package Models;

public class SubjectCategory {
    private int catId;
    private int subjectId;

    public SubjectCategory() {}

    public SubjectCategory(int catId, int subjectId) {
        this.catId = catId;
        this.subjectId = subjectId;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }
    
}
