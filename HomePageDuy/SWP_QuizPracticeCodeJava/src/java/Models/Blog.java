
package Models;

import java.sql.Date;

public class Blog {
    private int blogId;
    private String blogTitle;
    private BlogCate blogCate;
    private String thumbnail;
    private Date lastUpdated;
    private User author;
    private String briefInfo; 
    private String blogDetail;

    public Blog() {}

    public Blog(int blogId, String blogTitle, String thumbnail, Date lastUpdated, String briefInfo, String blogDetail, BlogCate blogCate, User author) {
        this.blogId = blogId;
        this.blogTitle = blogTitle;
        this.blogCate = blogCate;
        this.thumbnail = thumbnail;
        this.lastUpdated = lastUpdated;
        this.author = author;
        this.briefInfo = briefInfo;
        this.blogDetail = blogDetail;
    }

    
    
    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    public BlogCate getBlogCategory() {
        return blogCate;
    }

    public void setBlogCategory(BlogCate blogCategory) {
        this.blogCate = blogCategory;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getBriefInfo() {
        return briefInfo;
    }

    public void setBriefInfo(String briefInfo) {
        this.briefInfo = briefInfo;
    }

    public String getBlogDetail() {
        return blogDetail;
    }

    public void setBlogDetail(String blogDetail) {
        this.blogDetail = blogDetail;
    }

    @Override
    public String toString() {
        return "Blog{" + "blogId=" + blogId + ", blogTitle=" + blogTitle + ", blogCategory=" + blogCate + ", thumbnail=" + thumbnail + ", lastUpdated=" + lastUpdated + ", author=" + author + ", briefInfo=" + briefInfo + ", blogDetail=" + blogDetail + '}';
    }

    

}
