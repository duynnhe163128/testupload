
package Models;

import java.sql.Date;

public class Subject {
    private int subjectId;
    private String subjectName;
    private String description;
    private String thumbnailURL;
    private boolean featured ;
    private boolean status;
    private Date updatedDate;
    private Category cat;

    public Subject() {
    }

    public Subject(int subjectId, String subjectName, String description, String thumbnailURL, boolean featured, boolean status, Date updatedDate, Category cat) {
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.description = description;
        this.thumbnailURL = thumbnailURL;
        this.featured = featured;
        this.status = status;
        this.updatedDate = updatedDate;
        this.cat = cat;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnailURL() {
        return thumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL) {
        this.thumbnailURL = thumbnailURL;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Category getCat() {
        return cat;
    }

    public void setCat(Category cat) {
        this.cat = cat;
    }
    
    @Override
    public String toString() {
        return "Subject{" + "subjectId=" + subjectId + ", subjectName=" + subjectName + ", description=" + description + ", thumbnailURL=" + thumbnailURL + ", featured=" + featured + ", status=" + status + ", updatedDate=" + updatedDate + ", cat=" + cat + '}';
    }

    

    
    
}
