
package Models;

public class User {
    private int userId;
    private String fullName;
    private String userName;
    private String password;
    private int roleId;
    private String profilePic;
    private String email;
    private boolean gender;
    private String phone;
    private boolean status;

    public User() {}

    public User(String email) {
        this.email = email;
    }

    public User(int userId, String fullName) {
        this.userId = userId;
        this.fullName = fullName;
    }
    
    public User(int userId, String fullName, String userName, String password, int roleId, String profilePic, String email, boolean gender, String phone, boolean status) {
        this.userId = userId;
        this.fullName = fullName;
        this.userName = userName;
        this.password = password;
        this.roleId = roleId;
        this.profilePic = profilePic;
        this.email = email;
        this.gender = gender;
        this.phone = phone;
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", fullName=" + fullName + ", userName=" + userName + ", password=" + password + ", roleId=" + roleId + ", profilePic=" + profilePic + ", email=" + email + ", gender=" + gender + ", phone=" + phone + ", status=" + status + '}';
    }
    
}
