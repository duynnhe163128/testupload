
package Models;

public class Dimension {
    private int dimensionId;
    private String dimensionName;
    private String description;
    private int typeId;

    public Dimension() {}

    public Dimension(int dimensionId, String dimensionName, String description, int typeId) {
        this.dimensionId = dimensionId;
        this.dimensionName = dimensionName;
        this.description = description;
        this.typeId = typeId;
    }

    public int getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(int dimensionId) {
        this.dimensionId = dimensionId;
    }

    public String getDimensionName() {
        return dimensionName;
    }

    public void setDimensionName(String dimensionName) {
        this.dimensionName = dimensionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
    
}
