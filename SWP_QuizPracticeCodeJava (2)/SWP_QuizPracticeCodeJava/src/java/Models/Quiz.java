
package Models;

public class Quiz {
    private int quizId;
    private String quizName; 
    private int quizTypeId;
    private int subjectId;
    private int levelId;
    private String quizContent;
    private int numberOfQuestions;
    private int passRate;
    private int duration;

    public Quiz() {}

    public Quiz(int quizId, String quizName, int type, int level, String quizContent, int numberOfQuestions, int passRate, int duration, int quizSubject) {
        this.quizId = quizId;
        this.quizName = quizName;
        this.quizTypeId = type;
        this.subjectId = quizSubject;
        this.levelId = level;
        this.quizContent = quizContent;
        this.numberOfQuestions = numberOfQuestions;
        this.passRate = passRate;
        this.duration = duration;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public int getType() {
        return quizTypeId;
    }

    public void setType(int type) {
        this.quizTypeId = type;
    }

    public int getQuizSubject() {
        return subjectId;
    }

    public void setQuizSubject(int quizSubject) {
        this.subjectId = quizSubject;
    }

    public int getLevel() {
        return levelId;
    }

    public void setLevel(int level) {
        this.levelId = level;
    }

    public String getQuizContent() {
        return quizContent;
    }

    public void setQuizContent(String quizContent) {
        this.quizContent = quizContent;
    }

    public int getNumberOfQuestions() {
        return numberOfQuestions;
    }

    public void setNumberOfQuestions(int numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    public int getPassRate() {
        return passRate;
    }

    public void setPassRate(int passRate) {
        this.passRate = passRate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    
}
