package DAL;

import Models.User;
import Models.UserRole;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class UserDAO extends DBContext {

    public User userLogin(String email, String password) {
        String sql = " select * from dbo.[User]\n"
                + "  where [email] = ? \n"
                + "  and [password] = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getInt(5), rs.getString(6), rs.getString(7), rs.getBoolean(8), rs.getString(9), rs.getBoolean(10));
                return user;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<User> getAllUsers() {
        ArrayList<User> listUser = new ArrayList<>();
        String sql = " select * from dbo.[User]";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getInt(5), rs.getString(6), rs.getString(7), rs.getBoolean(8), rs.getString(9), rs.getBoolean(10));
                listUser.add(user);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return listUser;
    }
    
    public ArrayList<User> getUserEmail() {
        ArrayList<User> listUser = new ArrayList<>();
        String sql = " select email from dbo.[User]";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getString(1));
                listUser.add(user);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return listUser;
    }

    public boolean checkUserName(ArrayList<User> listUsername, String username) {
        for (User user : listUsername) {
            if (user.getUserName().equalsIgnoreCase(username)) {
                return true;
            }
        }
        return false;
    }
    public boolean checkEmail(ArrayList<User> listEmail, String email) {
        for (User user : listEmail) {
            if (user.getEmail().equalsIgnoreCase(email)) {
                return true;
            }
        }
        return false;
    }

    public void registerUser(String fullName, String username, String password,String email, int gender, String phone) {
        String sql = "INSERT INTO [dbo].[User]\n"
                + "           ([fullName],[userName]\n"
                + "           ,[password],[roleId]\n"
                + "           ,[profilePic],[email]\n"
                + "           ,[gender],[phone],[status])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,5\n"
                + "           ,'img/logo/avt2.png'\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,1)";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, fullName);
            stm.setString(2, username);
            stm.setString(3, password);
            stm.setString(4, email);
            stm.setInt(5, gender);
            stm.setString(6, phone);
            stm.executeUpdate();
        } catch (Exception e) {

        }
    }

    public ArrayList<UserRole> getUserRole() {
        ArrayList<UserRole> listRole = new ArrayList<>();
        String sql = "select * from UserRole";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                UserRole user = new UserRole(rs.getInt(1), rs.getString(2), rs.getBoolean(3));
                listRole.add(user);
            }
        } catch (Exception e) {

        }
        return listRole;
    }

    public User getUserByID(int userid) {
        String sql = "select * from [User] where userId = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4), 
                        rs.getInt(5), rs.getString(6), rs.getString(7), rs.getBoolean(8), rs.getString(9), rs.getBoolean(10));
                return user;
            }
        } catch (Exception e) {

        }
        return null;
    }
    
    public static void main(String[] args) {
        UserDAO udao = new UserDAO();
////        User u = udao.userLogin("DuongNHHE150328@fpt.edu.vn", "1");
////        System.out.println(u);
//        udao.registerUser("Nguyen Tung Duong", "duong1", "1", "duy0256@gmail.com", 0, "123456788");
////        System.out.println(udao.checkUserName(list, "Taylor"));;
////        
        ArrayList<User> listRole =  udao.getUserEmail();
        String email = "abc@gmail.com";
        if(udao.checkEmail(listRole, email) == false){
            System.out.println("Ko trùng mail");
        }else{
            System.out.println("trùng mail");
        }
    }
}
