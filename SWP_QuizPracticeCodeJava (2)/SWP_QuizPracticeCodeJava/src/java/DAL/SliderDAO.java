/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import Models.Blog;
import Models.Slider;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class SliderDAO extends DBContext {

    public ArrayList<Slider> getListSlider() {
        ArrayList<Slider> list = new ArrayList<>();
        String sql = "select * from Slider";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
               Slider s = new Slider(rs.getInt(1),
                       rs.getString(2),
                       rs.getString(3),
                       rs.getInt(4),
                       rs.getString(5),
                       rs.getBoolean(6));
               list.add(s);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }
    public static void main(String[] args) {
        SliderDAO s = new SliderDAO();
        System.out.println(s.getListSlider());
    }
}
