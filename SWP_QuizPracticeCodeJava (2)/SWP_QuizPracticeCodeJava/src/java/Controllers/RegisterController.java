package Controllers;

import SendEmail.SendEmail;
import SendEmail.Security;
import DAL.UserDAO;
import Models.User;
import Models.UserRole;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "RegisterController", urlPatterns = {"/signup"})
public class RegisterController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO udao = new UserDAO();
        ArrayList<User> list = udao.getUserEmail();
//        request.setAttribute("role", listRole);
        String fullName = request.getParameter("fullname");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String confirmPass = request.getParameter("confirmPass");
        String email = request.getParameter("email");
        String sex = request.getParameter("gender");
        String phone = request.getParameter("phone");

        int gen = 0;
        if (sex.equals("male")) {
            gen = 1;
        }
        SendEmail sendMail = new SendEmail();
        Security sec = new Security();
        
//        ArrayList<User> listUsername = udao.getAllUsers();
        if (udao.checkEmail(list, email) == false && password.equals(confirmPass)) {
            udao.registerUser(fullName, username, confirmPass, email, gen, phone);
            sendMail.send(email, sec.getSubject(), sec.getBody(), sec.getAddress(),sec.getPassword());
            request.getRequestDispatcher("login").forward(request, response);
        } else {
            
            response.sendRedirect("home");
        }
    }

}
