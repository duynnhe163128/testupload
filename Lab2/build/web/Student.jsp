<%@page import="java.util.ArrayList"%>
<%@page import="model.Student"%>
<%@page import="java.util.Random"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lab2</title>
        <%
            String num = (String) request.getAttribute("param");
            ArrayList<Student> students = (ArrayList) request.getAttribute("student_list");
        %>       
    </head>
    <body>
        <form action="StudentServlet" method ="post">
            Number of Students : <input type="text" name="id" value="" /> 
            <input type="submit" value="Generate" />
        </form>
        <table border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>DOB</th>
                    <th>Phone</th>
                    
                </tr>
            </thead>
            <tbody>
                <%
                    SimpleDateFormat sp = new SimpleDateFormat("d-M-YYYY");
                %>
                <% for (int i = 0; i < students.size(); i++) {%>
                <tr>
                    <td><%=i + 1%></td>
                    <td><%=students.get(i).getName()%></td>
                    <td>
                        <input type="checkbox" <%=students.get(i).isGender() ? "checked" : ""%>/>
                    </td>
                    <td>
                        <%=sp.format(students.get(i).getDob())%>
                    </td>
                    <td>
                        <%=students.get(i).getPhone()%>
                    </td>
                </tr>
                <%}%>
            </tbody>
        </table>

    </body>
</html>

