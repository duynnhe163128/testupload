/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Random;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Student;

/**
 *
 * @author admin
 */
public class StudentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Student s = new Student();
        String param = request.getParameter("id");
        if (param == null || param.equals("")) {
            s.setName("Blank");
        } else {
            s.setName("Data");
        }
        int num = Integer.parseInt(param);
        request.setAttribute("student_list", list(num));
        request.setAttribute("param", param);
        request.getRequestDispatcher("Student.jsp").forward(request, response);
    }

    public ArrayList<Student> list(int size) {
        ArrayList<Student> students = new ArrayList<Student>();
        Random rand = new Random();
        long currentDate = new Date().getTime();
        for (int i = 0; i < size; i++) {
            students.add(new Student(i, randomName(rand.nextInt(10) + 3),
                    rand.nextBoolean(),
                    new Date(Math.abs(rand.nextLong() % currentDate)),rand.nextInt(100)+100));
        }
        return students;
    }

    public String randomName(int len) {
        String character = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk";
        Random rand = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(character.charAt(rand.nextInt(character.length())));
        }
        return sb.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
